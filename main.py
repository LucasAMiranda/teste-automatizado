import time
from get_gecko_driver import GetGeckoDriver
from selenium import webdriver


get_driver = GetGeckoDriver()
get_driver.install()

driver = webdriver.Firefox()
driver.get("https://sensorweb.com.br/a-sensorweb/sobre-nos-sensorweb/")
time.sleep(3)
driver.get("https://sensorweb.com.br/a-sensorweb/parceiros/")
time.sleep(3)
driver.get("https://sensorweb.com.br/contato/fale-conosco/")
time.sleep(3)
driver.get("https://sensorweb.com.br/solucoes/")
time.sleep(3)
driver.get("https://sensorweb.com.br/clientes/")
time.sleep(3)

driver.quit()